__dir__=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
SPACK_PATH="${__dir__}/root/bin"

export MODULEPATH="${__dir__}/modules:${MODULEPATH}"

# Prepend `shared` to any command to prevent it from creating files without group write permissions.
#
# Example usage is calling Git to update Spack
#   `( cd root && shared git pull )``.
#
# Without `shared`, `git pull` results in `git` being unusable in this folder by other users.
# That is until permissions are corrected.
function shared() {
  (
    umask 0007  # instead of default 0022 on smaug and 0077 on gwdg
    "$@"
  )
}

function _temporary-spack-env() {
  (
    umask 0007  # instead of default 0022 on smaug and 0077 on gwdg
    . ${__dir__}/root/share/spack/setup-env.sh
    "$@"
  )
}

# Remove path passed as argument from the PATH environment variable.
# Found at https://stackoverflow.com/a/19587970
function _path_remove {
  PATH=${PATH/":$1"/}
  PATH=${PATH/"$1:"/}
}

function _spack-env() {
  # We cannot use `shared` wrapper here, because it resets environment. We need to handle umask manually.
  previous_umask=$(umask)
  umask 0007

  if ! command -v spack &> /dev/null; then
    # Prints Git version and Spack location
    echo "Initializing Spack [$(cd ${__dir__}/root && git describe --tags --match "v*")] at [${__dir__}]"
    echo

    # Supported way to initialize Spack
    . ${__dir__}/root/share/spack/setup-env.sh

    _path_remove "${SPACK_PATH}"  # We don't want `spack` command to be accessible.
  fi

  export PATH="${SPACK_PATH}:${PATH}"  # We need `spack` command. Absolute path cannot be used.

  "$@"

  _path_remove "${SPACK_PATH}"  # We don't want `spack` command to be accessible.

  umask "${previous_umask}"

  # Spack keeps being initialized, otherwise we cannot use packages loaded by `spack load`.
}

# It is a wrapper for Spack command with following tasks:
# - it passes correct config directory and disables `~/.spack` config directory
# - adds umask command, to keep permissions correct
# The rest are workarounds for 1) a bug when specifying config folder 2) Python 2 being default on GWDG.
function shared-spack() {
  # Issue: `--config-scope` doesn't work together with `load`/`unload` commands. It looks like a bug.
  # - Workaround below checks for `load` and `unload` inside of passed commands and alters command slightly.
  # - `SPACK_DISABLE_LOCAL_CONFIG=1` disables Spack looking into `~/.spack`, so we can have private Spack installations that use `~/.spack` directory without interference.
  if grep -qE "(un)?load" <<< "$@"; then
    SPACK_DISABLE_LOCAL_CONFIG=1 _spack-env spack "$@"
  else
    SPACK_DISABLE_LOCAL_CONFIG=1 _temporary-spack-env spack --config-scope "${__dir__}/config" "$@"
  fi
}

# Runs permission check inside of the Spack directory.
# It produces a list of files with invalid permisions including owners of these files.
function shared-spack-check-permissions() {
  (
    set -e
    cd "${__dir__}"

    diff <(find * ! -perm -600 | xargs -n1 ls -ld) <(find * ! -perm -060 | xargs -n1 ls -ld) || { echo ; echo "Ask owners to run [shared-spack-fix-permissions] command." ; return 1 ; }
    diff <(find .git ! -perm -600 | xargs -n1 ls -ld) <(find .git ! -perm -060 | xargs -n1 ls -ld) || { echo ; echo "Ask owners to run [shared-spack-fix-permissions] command." ; return 1 ; }
  )
}

# Sets group-write permission inside of Spack directory. It can only update files that belong to you.
# When offending file belongs to some other user, you need to ask that user to run `shared-spack-fix-permissions`.
# Fixing permissions is only necessary when somebody does direct operations on Spack folder without using `shared` prefix.
function shared-spack-fix-permissions() {
  (
    set -e
    cd "${__dir__}"

    find . -user $(whoami) ! -perm -660 | xargs -n1 chmod g=u
    find . -user $(whoami) ! -perm -660 | xargs -n1 chmod o-rwx
  )
}

function shared-spack-update() {
  ( cd ${__dir__}/root && shared git checkout develop && shared git pull )
}

function shared-spack-refresh-modules() {
  # GWDG has module cache, Smaug doesn't
  if [[ $(hostname) == "gwdu101" || $(hostname) == "gwdu102" || $(hostname) == "gwdu103" ]]; then
    shared-spack module tcl refresh -y --delete-tree && sleep 1 && module --ignore-cache avail
  else
    shared-spack module tcl refresh -y --delete-tree && module avail
  fi
}

function shared-spack-quota-usage() {
  du -sh "${__dir__}"
}
